<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesListRequest;
use App\Http\Requests\CategoriesUpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoriesListRequest $request
     * @return Collection
     */
    public function index(CategoriesListRequest $request): Collection
    {
        $data = $request->validated();
        $limit = isset($data['pageSize']) ? (int)$data['pageSize'] : 2;
        $page = isset($data['page']) ? (int)$data['page'] : 1;
        $offset = ($page - 1) * $limit;

        if ((isset($data['name']) && (isset($data['description']))) || isset($data['search'])) {
            unset($data['name']);
            unset($data['description']);
        }

        $direction = 'DESC';
        $field = 'createdDate';
        $available_fields = (new Category())->getFields();
        if (!empty($data['sort']) && in_array(ltrim($data['sort'], '-'), $available_fields)) {
           if($data['sort'][0] === '-') {
               $data['sort'] = ltrim($data['sort'], '-');
           } else {
               $direction = 'ASC';
           }
            $field = $data['sort'];
        }

        return Category::query()
            ->limit($limit)
            ->when(!empty($data['name']), function ($q) use ($data) {
                $q->where(DB::raw("REPLACE(LOWER(name), 'ё', 'е')"), 'LIKE', '%' . $this->prependSearch($data['name']) . '%');
            })
            ->when(!empty($data['description']), function ($q) use ($data) {
                $q->where(DB::raw("REPLACE(LOWER(description), 'ё', 'е')"), 'LIKE', '%' . $this->prependSearch($data['description']) . '%');
            })
            ->when(isset($data['active']), function ($q) use ($data) {
                $q->where('active', (boolean)$data['active']);
            })
            ->when(!empty($data['search']), function ($q) use ($data) {
                $q->where(function ($query) use ($data) {
                    $query->where(DB::raw("REPLACE(LOWER(name), 'ё', 'е')"), 'LIKE', '%' . $this->prependSearch($data['search']) . '%')
                        ->orWhere(DB::raw("REPLACE(LOWER(description), 'ё', 'е')"), 'LIKE', '%' . $this->prependSearch($data['search']) . '%');
                });

            })
            ->offset($offset)
            ->orderBy($field, $direction)
            ->get();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CategoriesCreateRequest $request
     * @return Category
     */
    public function store(CategoriesCreateRequest $request): Category
    {
        $category = Category::create($request->validated());

        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Category
     */
    public function show(int $id): Category
    {
       return Category::findOrFail($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return //Category
     */
    public function update(CategoriesUpdateRequest $request, int $id): Category
    {
        $category = Category::findOrFail($id);
        $data = $request->validated();
        $category->update($data);

        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy(int $id) :void
    {
        Category::findOrFail($id)->delete();
    }

    /**
     * Get category by slug
     *
     * @param string $slug
     * @return Category
     */
    public function getCategoryBySlug(string $slug) : Category
    {
        return Category::where('slug', $slug)->firstOrFail();
    }

    /**
     * Подготовка поисковой фразы
     *
     * @param string $string
     * @return string
     */
    protected function prependSearch(string $string) : string
    {
        return str_replace('ё', 'е', trim(strtolower(e($string))));
    }
}
