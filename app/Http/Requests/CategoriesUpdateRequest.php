<?php

namespace App\Http\Requests;


class CategoriesUpdateRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'active' => 'nullable|boolean',
            'slug' => 'nullable|unique:categories,slug|regex:/^[_\-A-Za-z\d]+$/i',
            'id' => 'nullable|integer|unique:categories,id|min:1',
            'createdDate' => 'nullable|date'
        ];
    }
}
