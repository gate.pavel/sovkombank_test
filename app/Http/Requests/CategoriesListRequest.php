<?php

namespace App\Http\Requests;

class CategoriesListRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'search' => 'nullable|string',
            'sort' => 'nullable|string',
            'page' => 'nullable|integer|min:0',
            'pageSize' => 'nullable|integer|min:1|max:9',
            'active' => 'nullable|boolean'
        ];
    }
}
