<?php

namespace App\Http\Requests;

class CategoriesCreateRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'active' => 'required|boolean',
            'slug' => 'required|unique:categories,slug|regex:/^[_\-A-Za-z\d]+$/i',
        ];
    }
}
