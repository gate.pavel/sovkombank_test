<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель категории
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property boolean $active
 * @property Carbon $createdDate
 */

class Category extends Model
{
    use HasFactory;

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = null;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'active',
        'createdDate',
        'id'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function getFields()
    {
        return $this->fillable;
    }
}
