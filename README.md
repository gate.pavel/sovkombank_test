**Описание**

В основе решения лежит пакет `Laravel sail`.

На его базе поднят контейнер с веб-сервером и PHP, а также база данный MySQL в отдельном контейнере.


**Установка**

Установка проекта довольно стандартная, обращение к docker-контейнерам идет через laravel sail.

Требования к установленному ПО для запуска: PHP 8.*, composer, docker, docker-compose

```shell
#Создаем файл .env с параметрами окружения
cp .env.example .env

#Устанавливаем пакеты php и Laravel Sail через composer
composer install

#Сборка и запуск контейнеров
./vendor/bin/sail up -d

#Миграции
./vendor/bin/sail artisan migrate

```


**Документация**

Документация в формате OpenAPI 3.0 находится в файле `openapi.yaml` в корне проекта.
